<?php
if (isset($_POST['submit'])){

$newFileName = $_POST['filename'];
if (empty($newFileName)){
    $newFileName = "gallery";
} else{
    $newFileName = strtolower(str_replace(" ","-", $newFileName));
}
$imageTitle = $_POST['filetitle'];
$imageDesc = $_POST['filedesc'];


$file = $_FILES['file'];

$fileName = $_FILES['file']['name'];
$fileTmpName = $_FILES['file']['tmp_name'];
$fileSize = $_FILES['file']['size'];
$fileError = $_FILES['file']['error'];
$fileType = $_FILES['file']['type'];

$fileExt = explode('.', $fileName); // de punt uit file naam halen
$fileActualExt = strtolower(end($fileExt)); // alles is kleine letters

$allowed = array('jpg', 'jpeg', 'png');

    if (in_array($fileActualExt, $allowed)) {      
          if ($fileError === 0) {
            if ($fileSize < 1000000){
                $imageFullName = uniqid('', true).".".$fileActualExt;
                $fileDestination = 'uploads/'.$imageFullName;

                include_once "config.php";

                if (empty($imageTitle) || empty($imageDesc)){
                    header("Location: adm-gallery.php?Error");  
                    exit();
                } else {
                   $sql = "SELECT * FROM gallery;"; //mogelijk error??!
                   $stmt = mysqli_stmt_init($conn);
                   if (!mysqli_stmt_prepare($stmt, $sql)){
                       echo "SQL statment failed!";
                   } else {
                       mysqli_stmt_execute($stmt);
                       $result = mysqli_stmt_get_result($stmt);
                       $rowCount = mysqli_num_rows($result);
                       $setImageOrder = $rowCount + 1;

                       $sql = "INSERT INTO gallery (titleGallery, descGallery, imgFullNameGallery, orderGallery) VALUES (?, ?, ?, ?);";
                       if (!mysqli_stmt_prepare($stmt, $sql)){
                        echo "SQL statment failed!";
                    } else{
                        mysqli_stmt_bind_param($stmt, "ssss", $imageTitle, $imageDesc, $imageFullName, $setImageOrder);
                        mysqli_stmt_execute($stmt);

                        move_uploaded_file($fileTmpName, $fileDestination);

                        header("Location: adm-gallery.php?upload=success");
                    }
                   }
                } 
            } else{
                echo "Te groot bestand, max 500MB!";
            } 
    } else{
        echo "Ongeldig bestand!"; 
    }
}
}