<?php
require 'articleadmin/app/start.php';

if (!empty($_POST)) {
    $id = $_POST['id'];
    $label = $_POST['label'];
    $body = $_POST['body'];
    
    $updatePage = $db->prepare("
        UPDATE articles
        SET
            label = :label,
            body = :body,
            updated = NOW()
        WHERE id = :id
    ");
    
    $updatePage->execute([
        'id' => $id,
        'label' => $label,
        'body' => $body,
    ]);
    
    header('Location: ' . BASE_URL . '/list.php');
}

/*if (!isset($_GET['id'])); {
    header('Location: ' . BASE_URL . '/list.php');
    die();
}*/

$page = $db->prepare("
    SELECT id, label, body, created, updated
    FROM articles
    WHERE id = :id
");
$page->execute(['id' => $_GET['id']]);
$page = $page->fetch(PDO::FETCH_ASSOC);


require 'articleadmin/app/views/admin/edit.php';

?>