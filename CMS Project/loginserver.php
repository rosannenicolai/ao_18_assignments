<?php
session_start();
$errors = array();

// database info
$db_host = 'localhost';
$db_user = 'root';
$db_pass = '';
$db_name = 'cms';

function redirect($DoDie = true) {
    header('Location: dashboard.php');
    exit();
}
if(isset($_SESSION['username'])) {
    redirect();
}



if ((isset($_POST['username'])) && (isset($_POST['password'])) ) {
    $username = $_POST['username'];
    $password = $_POST['password'];




$con = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

//Check if connection is available

if (mysqli_connect_error() ) {
    die ('Failed to connect to MySQL: ' . mysqli_connect_error());
}

// Check if data submitted is correct

if (($username == '') || ($password == '')) {
    $errors[] ='Missing username and/or password!';
} else {



// Prepare SQL

if ($stmt = $con->prepare("SELECT id, password FROM users WHERE username = ?")) {
  // Bind parameters (s = string, i = int, b = blob) Hash password using PHP password_hash
//$stmt = $con->prepare("SELECT id, password FROM users WHERE username = ?");
  $stmt->bind_param('s', $username);
  $stmt->execute();
  $stmt->store_result();

  //Store result to check if account exists in database
  if ($stmt->num_rows> 0) {
      $stmt->bind_result($id, $password);
      $stmt->fetch();

		      }

      if (password_verify($_POST['password'], $password)) {
        // Verification succes! User logged in!
          $_SESSION['loggedin'] = TRUE;
          $_SESSION['username'] = $_POST['username'];
          $_SESSION['id'] = $id;
          $_SESSION['success'] = "You are now logged in";

          header("Location: dashboard.php");

      }elseif (!password_verify($_POST['password'], $password)) {
        $errors[] = 'Incorrect username and/or password!';

	}
  $stmt->close();
}
}

  //$stmt->close();
//}
 //else {
	//echo 'Could not prepare statement!';


}
?>
