<html>
<head>
<?php include "navbar.php" ?>
<title> Articles </title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<h1> Articles </h1>

    <style>
    .date{
    float: right;
    text-align:right;
    }
</style>
 
    <?php  
$pages = $db->query("
    SELECT *
    FROM articles
    ORDER BY created DESC
")->fetchAll(PDO::FETCH_ASSOC)
    ?>
    <?php if(empty($pages)): ?>
        <p>Sorry, no pages added.</p>
    <?php else: ?>
        <ul>
            <?php foreach ($pages as $page): ?>
            <div class="w3-panel w3-red">
       <div class="container" style="width:1000px;"> 
           <div class="panel click">
               <li>    <a style="font-size: 35px;" href="<?php echo BASE_URL; ?>/page.php?page=<?php echo $page['id']; ?>"><?php echo $page['label'];?> <div class="date"><?php echo $page['created'];?></div></a></li>
               </div>
           </div>
      </div>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <?php include "footer.php" ?>




        



</body>
</html>