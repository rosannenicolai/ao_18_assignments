<html>
<head>
<?php include "navbar.php" ?>
<title> Articles </title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<h1> Articles </h1>
    <div class="w3-panel w3-red">
       <div class="container" style="width:1000px;"> 
           <div class="panel click">

    <?php if (!$page): ?>
        <p>No page found, ya messed up!</p>
    <?php else: ?>

        <h2><?php echo esc($page['label']); ?></h2> <?php //esc is javascript insertion protection(see functions) ?>

               <p style="white-space: pre"> <?php echo esc($page['body']); ?> </p>

        <p>
            <p style="color:#666666";> Created on <?php echo $page['created']->format('jS M Y'); ?></p>
            <?php if($page['updated']): ?>
               <p style="color:#666666";>Last updated <?php echo $page['updated']->format('jS M Y'); ?></p>
            <?php endif; ?>
        </p>

</div>
           </div>
      </div>
    <?php endif; ?>
    <?Php include "footer.php"?>
