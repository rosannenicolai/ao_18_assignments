<?php
    session_start();
    session_destroy();
    //Redirect them back to the home page or something.
    header('Location: homepage.php');
    exit;

?>
