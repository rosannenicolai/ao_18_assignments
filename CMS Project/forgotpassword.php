<?php
    require 'vendor/autoload.php';

    $host = '127.0.0.1';
    $db = 'cms';
    $user = 'root';
    $pass = '';
    $charset = 'utf8mb4';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $options =[
        PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES      => false
    ];

    try {
        $pdo = new PDO($dsn, $user, $pass, $options);
    } catch (PDOException $e) {
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }

    if(!isset($_GET['hash'])):
?>
<form method="post">
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>SB Admin - Forgot Password</title>

        <!-- Bootstrap core CSS-->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin.css" rel="stylesheet">

    </head>

    <body class="bg-dark">

    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Reset Password</div>
            <div class="card-body">
                <div class="text-center mb-4">
                    <h4>Forgot your password?</h4>
                    <p>Enter your email address and we will send you instructions on how to reset your password.</p>
                </div>







    <div class="form-group">
        <div>
            <label for="email"></label>
        </div>
        <div>
            <center>
                <input type="email" name="email" id="email" class="from-control" placeholder="Enter email address" required="required" autofocus="autofocus">
            </center>
        </div>
    </div>
    </center>



    <div class="field-group">
        <div>
            <center>
            <input type="submit" value="Submit" class="btn btn-primary">
            </center>
        </div>
    </div>
                <div class="text-center">
                    <a class="d-block small mt-3" href="register.php">Register an Account</a>
                    <a class="d-block small" href="login.php">Login Page</a>
                </div>
            </div>
        </div>
    </div>
</form>




<?php else: ?>

    <form method="post">
        <!DOCTYPE html>
        <html lang="en">

        <head>

            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">

            <title>SB Admin - Forgot Password</title>

            <!-- Bootstrap core CSS-->
            <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

            <!-- Custom fonts for this template-->
            <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

            <!-- Custom styles for this template-->
            <link href="css/sb-admin.css" rel="stylesheet">

        </head>

        <body class="bg-dark">

        <div class="container">
            <div class="card card-login mx-auto mt-5">
                <div class="card-header">Reset Password</div>
                <div class="card-body">
                    <div class="text-center mb-4">
                        <h4>Reset your password</h4>
                        <p>Enter your new password and return to the login page to login to your account.</p>
                    </div>
        <div class="form-group">
            <div>
                <label for="password"></label>
            </div>
            <div>
                <center>
                <input type="password" name="password" id="password" class="form-group" placeholder="Enter new password" required="required" autofocus="autofocus">
                </center>
            </div>
        </div>
        <div class="form-group">
            <div>
                <center>
                <input type="submit" value="Submit" class="btn btn-primary">
                </center>''
            </div>
        </div>
        <div class="text-center">
                    <a class="d-block small" href="login.php">Login Page</a>
    </form>


<?php endif; ?>
<?php
if(!empty($_POST["email"])){

    $stmt = $pdo->prepare('SELECT * FROM `users` WHERE email = :email');
    $stmt->execute(['email' => $_POST['email']]);
    $user = $stmt->fetch();

    if(empty($user)) {
        die('User not found');
    } else {

        $hash = bin2hex(random_bytes(16));

        $stmt = $pdo->prepare('INSERT INTO `password_resets` (email, hash) VALUES (:email, :hash)');
        $stmt->execute(['email' => $_POST['email'], 'hash' => $hash]);

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom('noreply@scrumbags.com', 'The Scrumbags');
        $email->setSubject('Your password reset');
        $email->addTo($_POST['email'], $user['username']);
        $email->addContent('text/html', '<p>Hi ' . $user['username'] . ',</p><p>You have requested a password reset, here is a link to change your password:</p><a href="http://localhost/localhost/cms%20darren/forgotpassword.php?hash=' . $hash . '">Click here to change your password.</a>');

        $sendGrid = new SendGrid('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');

        try {
            $response = $sendGrid->send($email);

        } catch (Exception $e) {
            die($e->getMessage());
        }

    }

} elseif(!empty($_POST['password'])) {

    $stmt = $pdo->prepare('SELECT * FROM `password_resets` WHERE hash = :hash');
    $stmt->execute(['hash' => $_GET['hash']]);
    $data = $stmt->fetch();

    if(empty($data)) {
        die('The hash is invalid');
    } else {

        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
        $stmt = $pdo->prepare('UPDATE `users` SET password = :password WHERE email = :email');
        $stmt->execute(['password' => $password, 'email' => $data['email']]);

        die('Your password has been reset');

    }

}