<html>
<head>
<?php include "navbar.php" ?>
<title> Gallery </title>
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<h1> Gallery</h1>
    
    <?php
    include_once 'config.php';

    $sql = "SELECT * FROM gallery ORDER BY orderGallery DESC";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo "SQL statement failed!";
    } else {
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);

        while ($row = mysqli_fetch_assoc($result)) {
          echo '<div class="gallery">
          <a href="uploads/'.$row["imgFullNameGallery"].'"><img src="uploads/'.$row["imgFullNameGallery"].'"</a>
          </a>
          <div class ="title"<h2>'.$row["titleGallery"].'</h2></div>
          <div class="desc"<p>'.$row["descGallery"].'</p></div>
          </div>'; 
        }
    }
    ?>
<?php include "footer.php" ?>

</body>
</html>
