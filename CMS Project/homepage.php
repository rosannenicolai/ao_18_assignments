<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" > 
	<title>The Scrumbags</title>
</head>
<body>
<?php include "navbar.php" ?>
<body background="https://i.imgur.com/ItMuoxT.jpg">

	<div id="clockbox" class="clock"></div>

	<script type="text/javascript">
		var tmonth = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		function GetClock(){
			var d = new Date();
			var nmonth = d.getMonth(), ndate = d.getDate(), nyear = d.getFullYear();

			var nhour = d.getHours(), nmin = d.getMinutes(), nsec = d.getSeconds();

			if(nmin <= 9) nmin="0"+nmin;
			if(nsec <= 9) nsec="0"+nsec;

			var clocktext = "" + tmonth[nmonth] + " " + ndate + ", " + nyear + " " + nhour + ":" + nmin + ":" + nsec + "";
			document.getElementById('clockbox').innerHTML=clocktext;
		}

		GetClock();
		setInterval(GetClock,1000);
	</script>

<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
	if (window.pageYOffset > sticky) {
		header.classList.add("sticky");
	} else{
		header.classList.remove("sticky");
	}
}
</script>



<?php include "footer.php" ?>

</body>
</html>