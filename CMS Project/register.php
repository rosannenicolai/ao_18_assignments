<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Darren van Klaveren">

    <title>SB Admin - Register</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body class="bg-dark">
    <div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">Register an Account</div>
        <div class="card-body">
          <form action="" method="POST">
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" id="username" name="username" class="form-control" placeholder="username" required="required" autofocus="autofocus">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="text" id="email" class="form-control" placeholder="email address" required="required">
              </div>
            </div>
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="text" id="password" name="password" class="form-control" placeholder="Password" required="required">
                  </div>
                </div>
              </div>
            </div>
            <a class="btn btn-primary btn-block" ><input type="submit" name="submit" value="submit"></input>Register</a>
          </form>
          <div class="text-center">
            <a class="d-block small mt-3" href="login.php">Login Page</a>
            <a class="d-block small" href="forgotpassword.php">Forgot Password?</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <?php
      //variables voor de form
      $dbname = 'cms';
      $dbhost = 'localhost';
      $user = 'root';
      $pass = '';
      $username= '';
      $password = '';
      $email = '';
      $Loginname = '';
      $data = array("$username","$password","$email");

        //voor actie uit waneer er op submit word gedrukt tot aan line 135
            if (isset($_POST['submit'])) {

var_dump($_POST['username']);

            try {
                $database = new PDO("mysql:host=$dbhost;dbname=$dbname", $user, $pass);
                $database->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo $e->getMessage();
            }   

        // form if statement voor inserten in atabase 
          if(isset($_POST['username'])){
            $username = $_POST['username'];
              }else{
            $username = NULL;
          }
          if(isset($_POST['password'])){
            $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
              }else{
            $password = NULL;
          } 
          if(isset($_POST['email'])){
            $email = $_POST['email'];
              }else{
            $email = NULL;
          }

          if (empty($_POST) === false) {
            $required_fields = array('username', 'password', 'email');
            foreach ($_POST as $key=>$value) {
              if (empty($value) && in_array($key, $required_fields) === true) {
                $errors[] = "vull alles in AUB";
                break 1;
              }
            }

        // variable voor de check voor email exisst
            $stmt = $database->prepare("SELECT email FROM users WHERE email = :email");
              $stmt->bindValue(':email', $email);
        // if-statement voor de email exists
            $stmt->execute();
            if($stmt->rowCount() > 0){
                  echo "email bestaat al!";
              } else {

                var_dump($username);
                echo "u heeft zich succesvol regristeerd";
              $query = "INSERT INTO `users` (`username`, `password`, `email`) VALUES (?, ?, ?)";
              $insert = $database->prepare($query);
              $data = array($username, $password, $email);
              $insert->execute($data);
              }
          }
      } 

      ?>
  </body>
</html>

