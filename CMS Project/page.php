<?php

require 'articleAdmin/app/start.php';

if (empty($_GET['page'])) {
    $page = false;
} else {
    $idcheck = $_GET['page'];
    
    $page = $db->prepare("
        SELECT *
        FROM articles
        WHERE id = :id
        LIMIT 1
    ");
    
    $page->execute(['id' => $idcheck]);
    
    $page = $page->fetch(PDO::FETCH_ASSOC);
    
    if($page) {
        $page['created'] = new DateTime($page['created']); //makes date in database from string to DateTime
        
        if ($page['updated']){
            $page['updated'] = new DateTime($page['updated']); //makes date in database from string to DateTime
        }
    }
}

require VIEW_ROOT . '/page/show.php';