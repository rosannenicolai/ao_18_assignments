<?php
//setting header to json
header('Content-Type: application/json');

//database
define('DB_HOST', '127.0.0.1');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'CMS');

//get connection
$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

if(!$mysqli){
  die("Connection failed: " . $mysqli->error);
}

//adding new visitor
$visitor_ip = $_SERVER['REMOTE_ADDR'];

//checking if visitor is unique
$query = "SELECT * FROM visitors WHERE ip_address = '$visitor_ip'";
$result = mysqli_query($connection, $query);

//retrieving existing visitors
$query = "SELECT * FROM visitors";
$result = mysqli_query($connection, $query);

//checking query error

if (!$result) {
  die("Retrieving Query Error<br>".$query)''
}
$total_visitors = mysqli_num_rows($result);
if ($total_visitors < 1) {
  $query = "INSERT INTO visitors(ip_address) VALUES('$visitor_ip')";
  $result = mysqli_real_query($connection , $query);
}


//free memory associated with result
$result->close();

//close connection
$mysqli->close();

//now print the data
print json_encode($data);
?>
