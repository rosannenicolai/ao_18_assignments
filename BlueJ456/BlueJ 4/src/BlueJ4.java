import java.util.*;
import java.lang.String;
public class BlueJ4
{
    public static void main(String[] args)
    {
        int input, input2=0, intIn=0, intIn2=0;
        String inString = "";
        String dispString1 = "";
        String dispString2 = "";
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("------------------------------");
        System.out.println("Converting ........ to ........");
        System.out.println("---Pick your first type---");
        System.out.println("1. Integer");
        System.out.println("2. Octal");
        System.out.println("3. Hexadecimal");
        System.out.println("4. Binary");
        
        input = scanner.nextInt();
        
        
        if(input<1 || input>4)
        {
            System.out.println("Thats not possible, dumbass!");
        }
        
        switch(input)
        {
            case 1:
            dispString1 = "INTEGER";
            break;
            
            case 2:
            dispString1 = "OCTAL";
            break;
            
            case 3:
            dispString1 = "HEXADECIMAL";
            break;
            
            case 4:
            dispString1 = "BINARY";
            break;
            
            default:
            System.out.println("Something fucked up");
        }
        
        System.out.println("------------------------------");
        System.out.print("Converting ");
        System.out.print(dispString1);
        System.out.print(" to ........");
        System.out.println("");
        System.out.println("---Pick your second type---");
        System.out.println("1. Integer");
        System.out.println("2. Octal");
        System.out.println("3. Hexadecimal");
        System.out.println("4. Binary");
        
        Scanner scanner2 = new Scanner(System.in);
        input2 = scanner2.nextInt();
        
        switch(input2)
        {
            case 1:
            dispString2 = "INTEGER";
            break;
            
            case 2:
            dispString2 = "OCTAL";
            break;
            
            case 3:
            dispString2 = "HEXADECIMAL";
            break;
            
            case 4:
            dispString2 = "BINARY";
            break;
            
            default:
            System.out.println("Something fucked up");
        }
        
        System.out.println("------------------------------");
        System.out.print("Converting ");
        System.out.print(dispString1);
        System.out.print(" to ");
        System.out.print(dispString2);
        System.out.println("");
        System.out.print("Please enter ");
        if(input == 1 || input == 2)
        {
            System.out.print("an ");
        }
        else
        {
            System.out.print("a ");
        }
        System.out.print(dispString1);
        System.out.print(" number:");
        System.out.println("");
        
        if(input == 1)
        {
            Scanner SCint = new Scanner(System.in);
            intIn = SCint.nextInt();
        }
        else
        {
            Scanner SCstring = new Scanner(System.in);
            inString = SCstring.next();
        }
        
        if(input == 2)
        {
            if (inString.matches("[0-9]+") == false)
            {
               System.out.println("------------------------------");
               System.out.println("An octal number can only contains numbers.");
               return;
            }
        }
        
        if(input == 4)
        {
            if((inString.indexOf("0")==-1) && (inString.indexOf("1")==-1))
            {
               System.out.println("------------------------------");
               System.out.println("A Binary number can only contain 1's and 0's");
               return;
            }
        }
        
        //---------------UP INPUTS ---- DOWN CALCULATIONS--------------------
        
        System.out.println("------------------------------");
        System.out.print("Converting ");
        System.out.print(dispString1);
        System.out.print("(");
        if(input == 1)
        {
            System.out.print(intIn);
        }
        else
        {
            System.out.print(inString);
        }
        System.out.print(") to ");
        System.out.print(dispString2);
        System.out.print(".");
        System.out.println("");
        System.out.println("Your answer is: ");
        
        if(input == input2)
        {
            if(input == 1)
            {
               System.out.println(intIn);
               return;
            }
            else
            {
                System.out.println(inString);
                return;
            }
        }
            
        switch(input)
        {
            case 1:
            switch(input2)
            {               
               case 2:
               String result= Integer.toString(intIn,8);
               System.out.println(result);
               break;
               
               case 3:
               System.out.println(Integer.toHexString(intIn));
               break;
               
               case 4:
               System.out.println(Integer.toBinaryString(intIn));
               break;
               
               default:
               System.out.println("Dammit, This doesnt look good.");
            }
            break;
            
            case 2:
            switch(intIn)
            {
               case 1:
               int result= Integer.parseInt(inString,8);
               System.out.println(result);
               break;            
               
               case 3:
               int Oct2Hex = Integer.parseInt(inString,8);
               System.out.println(Integer.toHexString(Oct2Hex));
               break;
               
               case 4:
               int Oct2Bin = Integer.parseInt(inString,8);
               System.out.println(Integer.toBinaryString(Oct2Bin));
               break;
               
               default:
               System.out.println("Dammit, This doesnt look good.");
            }
            break;
            
            case 3:
            switch(intIn)
            {
               case 1:
               System.out.println(Integer.parseInt(inString,16));               
               break;
               
               case 2:              
               int Hex2Oct = (Integer.parseInt(inString,16));
               String result= Integer.toString(Hex2Oct,8);
               System.out.println(result);
               break;               
               
               case 4:
               int Hex2Bin= (Integer.parseInt(inString,16));;
               System.out.println(Integer.toBinaryString(Hex2Bin));
               break;
               
               default:
               System.out.println("Dammit, This doesnt look good.");
            }
            break;
            
            case 4:
            switch(intIn)
            {
               case 1:
               System.out.println(Integer.parseInt(inString,2));               
               break;
               
               case 2:              
               int Bin2Oct = (Integer.parseInt(inString,2));
               String result= Integer.toString(Bin2Oct,8);
               System.out.println(result);
               break;
               
               case 3:           
               int Bin2Hex = (Integer.parseInt(inString,2));
               System.out.println(Integer.toHexString(Bin2Hex));
               break;               
               
               default:
               System.out.println("Dammit, This doesnt look good.");
            }
            break;
            
            default:
            System.out.println("Uh Oh, Something broke!");
        }                
        System.out.println("-----------------------------------");
    }
}