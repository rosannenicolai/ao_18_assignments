/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluej5;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;
import java.math.BigDecimal;


public class BlueJ5 {


    public static void main(String[] args) {

        
       Pattern pattern = Pattern.compile("([\\d]+)([\\+\\-\\*\\/])([\\d]+)");
       Scanner scan = new Scanner(System.in);
        
        System.out.println("Welcome to the caluculator!\nYou can add, substract, multiply and divide. If you want to quit just type 'quit'.\nEnter your sum below:");
        String calc = scan.nextLine();


Matcher matcher = pattern.matcher(calc);

if (matcher.matches()) {

    int lhs = Integer.parseInt(matcher.group(1));
    int rhs = Integer.parseInt(matcher.group(3));
    
    BigDecimal left = new BigDecimal(lhs);
    BigDecimal right = new BigDecimal(rhs);

    char operator = matcher.group(2).charAt(0);

    BigDecimal result = BigDecimal.ZERO;
    


    //double result;
    switch (operator) {

        case '+': {
             result = (left.add(right));
             break;
            
        }

        case '-': {
            result = (left.subtract(right));
            break;
        }
        case '*': {
            result = (left.multiply(right));
            break;
        }
        case '/': {
            result =(left.divide(right));
            break;
        }
    }
    
    System.out.print(left + " " + operator + " " + right + " = " + result);
} 
        
    }
    
}
