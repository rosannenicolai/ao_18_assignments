/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bluej6;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author rosannenicolai
 */
public class BlueJ6 {

    /**
     * @param args the command line arguments
     */

   
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the factorial");
        long input = Integer.parseInt(reader.nextLine());

            int i;
            long factorial = 1;
            long startTime = System.nanoTime();
            for(i=1;i<=input;i++){    
            factorial=factorial*i;
        
    }
       
    System.out.println("Factorial of " + input + " is: " + factorial); 
    long endTime = System.nanoTime();
    long totalTime = endTime - startTime;
    long result1 = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
        System.out.println ("Total time in seconds: " + result1);
    
    startTime = System.nanoTime();
        while (i <=input) {
        factorial = factorial * i;
        i++;
        }
        System.out.println("Factorial of " + input + " is: " + factorial);
    endTime = System.nanoTime();
    totalTime = endTime - startTime;
    long result2 = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
        System.out.println ("Total time in seconds: " + result2);
        
        startTime = System.nanoTime();
        factorial = 1;
        i = 1;
        do {
            factorial = factorial * i;
            i++;
        }while (i <= input);
        
        System.out.println("Factorial of " + input + " is: " + factorial);
        endTime = System.nanoTime();
        totalTime = endTime - startTime;
        long result3 = TimeUnit.SECONDS.convert(totalTime, TimeUnit.NANOSECONDS);
        System.out.println ("Total time in seconds: " + result3);
        

    double fastest = Math.min(result1, Math.min(result2, result3));
        System.out.println("The Fastest method was: " + fastest);
}
}

