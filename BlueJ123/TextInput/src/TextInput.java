/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rosannenicolai
 */
import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
        

public class TextInput {
    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        System.out.println("Type something here:");
        String input = reader.nextLine();
        
        
        File textinputjava = new File("/Users/rosannenicolai/Documents/School/AO_2018/Periode 1/Sprint 1/BlueJ/textinputjava.txt");
        textinputjava.getParentFile().mkdirs();
        textinputjava.createNewFile();
        PrintWriter output = new PrintWriter(textinputjava);
        output.println(input);
        output.close();
    }
    
}
