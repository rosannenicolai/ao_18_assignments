/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rosannenicolai
 */
import java.util.Scanner;

public class LengtOfString {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in); 
        
        System.out.print("Enter a word: ");
        String givenWord = reader.nextLine();
        
        System.out.println("String length: " + givenWord.length());
        
    }
    
}
