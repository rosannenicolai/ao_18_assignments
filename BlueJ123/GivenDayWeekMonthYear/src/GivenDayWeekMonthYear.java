/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rosannenicolai
 */
import java.util.Scanner;

public class GivenDayWeekMonthYear {
    public static void main(String[] args) {
        
        int input, day, week, month, year;
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Enter the number of days: ");
        input = scan.nextInt();
        year = input / 365;
        input = input % 365;
        System.out.println(year + " year(s)");
        month = input / 12;
        input = input % 12;
        System.out.println(month + " month(s)");
        week = input / 7;
        input = input % 7;
        System.out.println(week + " week(s)");
        day = input;
        System.out.println(day + " day(s)");
    }
    
}
