#include <iostream>

using namespace std;

struct Date {
    int year;
    int month;
    int day;
};



bool isLeap(int year) {


    bool isLeap = false;

    //nested if loop to determine if year is leap or not
    if (year % 4 == 0) {
        if (year % 100 == 0) {
            if (year % 400 == 0) {
                isLeap = true;
            }
        }
        else isLeap = true;
    }
    //Returning isLeap
    return isLeap;
}

int monthLength(int year, int month) {

    //Array for months. If year is a leap year, 28 turns to 29.
    int monthLength[12] = { 31, (isLeap(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    //Returning the months - 1. Because array starts at 0.
    return monthLength[month - 1];

}

int dayOfYear(Date date) {

    //Array that has all the days in the months in it
    int dayOfYear[12] = { 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};



    if (isLeap(date.year) && date.month > 0) {

        //If year is a leap year and the month comes after January; add 1 to the value in the array.
        //Return date.month -2 (because that somehow works) + the days inserted + 1.

        return dayOfYear[date.month-2] + (date.day +1) ;
    } else {


        //If no leap year, or leap year and January, return date.month -2 + the days inserted.
        return dayOfYear[date.month-2] + date.day;
    }


}



int main(void)
{
    Date d;
    cout << "Enter year month day: ";
    cin >> d.year >> d.month >> d.day;
    cout << dayOfYear(d) << endl;
    return 0;
}