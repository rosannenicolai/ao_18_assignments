#include <iostream>

using namespace std;


int main(void) {

    int vector1[7] = {4, 7, 2, 8, 1, 3, 0};
    int vector2[7];
    int input;

    cout << "Enter the rotation factor: ";
    cin >> input;

    for (int i = 6; i >= 0; i--) {

        if (i + input >= 7) {
            vector2[i+input - 7] = vector1[i];
        } else {

            vector2[i+input] = vector1[i];
        }
    }


    for(int i = 0; i < 7; i++)
        cout << vector2[i] << ' ';
        cout << endl;

        return 0;

}