#include <iostream>

using namespace std;

int main(void) {

    unsigned short int val, revVal = 0, i = 0;
    bool ispalindrome = false;

    cout << "value = ";
    cin >> val;


while (i > 0) {

    revVal = (revVal << 1) | (i & 1);

    i = i >> 1;
}

if (revVal == val) {
    ispalindrome = true;
}



if(ispalindrome)
    cout << val << " is a bitwise palindrome" << endl;
else
cout << val << " is not a bitwise palindrome" << endl;

return 0;
}
