#include <iostream>

using namespace std;

int main() {

    long i, n, factorial = 1;

    cout << "Enter a positive number: ";
    cin >> n;

    for (i = 1; i <= n; i++) {
        factorial = factorial * i;
    }


    cout << "Factorial of " << n << " is: " << factorial;

    return 0;
}