#include <iostream>

using namespace std;

int main() {

    int oddNumber;

    cout << "Enter an odd number: ";
    cin >> oddNumber;

    int amount = oddNumber * oddNumber;
    int result = oddNumber + oddNumber - 2;

    cout << amount - result;

    return 0;
}