#include <cmath>
#include <iostream>
#include <iomanip>

using namespace std;

int main(void) {

    int sys;

    float m, ft, in;

    cout << "Type 0 for metric and 1 for imperial: ";
    cin >> sys;

    ft = 0;
    in = ft * 12 + in;
    m = in / 39.370;


    if (sys == 0) {
        cout << "how many meters? ";
        cin >> m;

        in = m * 39.37;

        while (in > 12) {
            if (in > 12) {

                in = (in - 12);
                ft++;
            }
        }

        cout << m << " meter converted to feet and inches is " << setprecision(5) << ft << "'" << in << "\"";


    } else if (sys == 1) {
        cout << "how many feet? ";
        cin >> ft;

        cout << "How many inches? ";
        cin >> in;

        in = in / 12;
        ft = ft + in;
        m = ft * 0.3048;

        cout << setprecision(0) << ft << "'" << in << "\"" << " converted to meters: " << setprecision(4) << m << "m";



    } else {
        cout << "Please enter a valid number";
    }



    // Insert your code here

    return 0;
}