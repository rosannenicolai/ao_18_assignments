#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <math.h>

using namespace std;

int main() {
    int inputOne, inputTwo;

    cout << "Enter 2 numbers:" << endl;
    cin >> inputOne >> inputTwo;



    float one = (1 / (float)inputOne);
    float two = (1 / (float)inputTwo);
    float epsilon = 0.000001f;
    float diff = fabs(one - two);

    if ( diff < epsilon ) {
        cout << "Results are equal (by 0.000001 epsilon)" << endl;
    } else {
        cout << "Results are not equal (by 0.000001 epsilon)" << endl;
    }



}