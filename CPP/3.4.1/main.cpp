#include <iostream>
using namespace std;



bool isLeap(int year) {

    // Insert your code here

    //set isLeap to false
    bool isLeap = false;

    //nested if loop to determine if year is leap or not
    if (year % 4 == 0) {
        if (year % 100 == 0) {
            if (year % 400 == 0) {
                isLeap = true;
        }
    }
    else isLeap = true;
}
//Returning isLeap
return isLeap;


}
int main(void) {


    for(int year = 1995; year < 2017; year++)
        cout << year << " -> " << isLeap(year) << endl;
    return 0; }