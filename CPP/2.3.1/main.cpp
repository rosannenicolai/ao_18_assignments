#include <iostream>

using namespace std;

int main() {

    int c0, steps;

    cout << "Enter a number that is not negative and not a zero: ";
    cin >> c0;

    steps = 0;


    while (c0 != 1) {

        if (c0 % 2 == 0) {
            c0 = c0 / 2;
            steps = steps + 1;
            cout << c0 << endl;
        } else if (c0 % 2 != 0) {
            c0= 3 * c0 +1;
            cout << c0 << endl;
            steps = steps + 1;
        }

    }

    cout << "steps = " << steps;

    return 0;
}