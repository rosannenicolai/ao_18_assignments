#include <iostream>
#include<stdio.h>

using namespace std;


unsigned long countSetBits(long n) {

    unsigned long count = 0;

    while (n) {

        n &= (n -1);
        count ++;

    }

    return count;
}

int main() {


    int input;
    cout << "Enter a number: ";
    cin >> input;

    cout << countSetBits(input);

    return 0;
}