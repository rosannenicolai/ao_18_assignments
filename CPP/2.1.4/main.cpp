#include <iostream>

using namespace std;

int main() {

    int year, month, day, result;

    cout << "Enter a year, month and day: ";
    cin >> year >> month >> day;

    month = month - 2;

    if (month < 0) {
        month = month + 12;
        year = year - 1;
    }

    month = (month * 83) / 32;
    month = month + day;
    month = month + year;
    month = month + (year / 4);
    month = month - (year / 100);
    month = month + (year / 400);
    month = month % 7;
    result = month;

    cout << "weekday number is: " << result;

    return 0;
}