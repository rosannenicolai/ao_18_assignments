#include <iostream>
#include <iomanip>

using namespace std;

int main(void) {
    float inputOne, inputTwo, inputThree, inputFour, inputFive;


    cout << "Enter 5 floating numbers" << endl;
    cin >> inputOne >> inputTwo >> inputThree >> inputFour >> inputFive;

    cout << inputOne << endl;
    std::cout << std::setprecision(2) << fixed << inputTwo << endl;

    std::cout << std::setprecision(6) << inputThree << endl;
    std::cout << std::setprecision(2) << inputFour << endl;
    cout << (int)inputFive << endl;
    // << inputTwo << endl << inputThree << endl<< inputFour << endl << (int)inputFive << endl;
}