#include <iostream>

using namespace std;

int main(void) {


    int n;

    cout << "enter a positive integer value between 1 and 50: ";
    cin >> n;

    if (n < 1 || n > 50) {
        cout << "Error: the amount entered is either too big or too small";
        return 0;
    }

    cout << '+';


    for(int i = 0; i < n; i++)

        cout << '-';
        cout << '+' << endl;

        for(int i = 0; i < n; i++) {

            cout << '|';

            for(int j = 0; j < n; j++)

                cout << ' ';

            cout << '|' << endl;
}

    cout << '+';

        for(int i = 0; i < n; i++)

        cout << '-';

    cout << '+' << endl; return 0;
}