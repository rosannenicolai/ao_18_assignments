cmake_minimum_required(VERSION 3.12)
project(2_3_5)

set(CMAKE_CXX_STANDARD 17)

add_executable(2_3_5 main.cpp)