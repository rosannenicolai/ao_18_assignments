#include <iostream>

using namespace std;

int main() {

    int year, a = 0, b = 0, c = 0, d = 0, e = 0;

    cout << "Welcome to the Easter calculator" << endl;
    cout << "Please enter a year: ";
    cin >> year;

    a = year % 19;
    b = year % 4;
    c = year % 7;
    d = ((a * 19) + 24) % 30;
    e = ((2 * b ) + (4 * c) + (6 * d) + 5) % 7;

    if ((d + e) < 10) {

        cout << "Easter falls on the " << (d + e + 22) << " day of March";

    } else {

        cout << "Easter falls on the " << (d + e - 9) << " day of April";

    }


    return 0;
}