#include <iostream>
#include <iomanip>

using namespace std;

int main() {


    int n;
    double result = 1;

    cout << "Enter a number: ";
    cin >> n;

    for (int i = 0; i < n; i++) {

        result = result / 2;

    }

    cout << "The result is: " << setprecision(20) << result;

    return 0;
}