#include <iostream>

using namespace std;

int main(void) {

    int inputOne, inputTwo, inputThree, inputFour;

    cout << "Enter four numbers between 1 and 255" << endl;
    cin >> inputOne >> inputTwo >> inputThree >> inputFour;

    if ((inputOne >= 1 && inputOne <= 255) && (inputTwo >= 1 && inputTwo <= 255) && (inputThree >= 1 && inputThree <= 255) && (inputFour >= 1 && inputFour <= 255)) {

        cout << inputOne << "." << inputTwo << "." << inputThree << "." << inputFour << endl;
    } else {
        cout << "You enter a number that's not between 1 and 255";
    }

}