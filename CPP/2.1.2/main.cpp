#include <iostream>

using namespace std;

int main(void) {

    float grossprice, taxrate, netprice, taxvalue;

    cout << "Enter a gross price: "; cin >> grossprice;
    cout << "Enter a tax rate: "; cin >> taxrate;

    taxvalue = (grossprice / (taxrate + 100)) * taxrate;
    netprice = (grossprice / (taxrate + 100)) * 100;

    if ((grossprice > 0) && (taxrate > 0 && taxrate < 100)) {

        cout << "Net price: " << netprice << endl;
        cout << "Tax value: " << taxvalue << endl;
    } else {
        cout << "You should enter a gross price higher than 0 and a taxrate between 0 and 100." << endl;
        cout<< "Please try again";
    }
    return 0;
}