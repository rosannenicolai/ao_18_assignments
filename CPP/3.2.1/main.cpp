#include <iostream>
#include <algorithm>

using namespace std;


int main(void) {

    int vector[] = { 3, -5, 7, 10, -4, 14, 5, 2, -13 };
    int n = sizeof(vector) / sizeof(vector[0]);

    //Points to beginning of vector
    int *ptr = vector;

    //First element is minimal
    int min = *ptr;
    //Skip to next element
    ptr++;

    for (int i = 1; i < n; i++) {
    // if current element is less than current minimal
        if (*ptr < min) {
    //Update min
            min = *ptr;
        }
    //Skip to next element
        ptr++;



    }

    cout << min;


    return 0; }

