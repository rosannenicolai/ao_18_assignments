#include <iostream>

using namespace std;

int main(void) {
    double pi = 3.14159265359;
    double x,y,sumFirst,sumSecond;


    cout << "Enter value for x: ";
    cin >> x;

    sumFirst = (x * x) / (pi * pi * ((x * x) + 0.5));
    sumSecond = 1 + (x * x) / (pi * pi * (((x * x) - 0.5) * ((x * x) - 0.5)));

    y = sumFirst * sumSecond;

    cout << "y = " << y;
    return 0;
}