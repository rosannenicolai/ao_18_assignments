#include <iostream>
#include <limits>

using namespace std;


struct TIME { int hour, minute;} Start_Time;
float Event_Time;
int i, j, k, l;

int main() {

    cout << "Using the 24h military format, please enter the start time in hours and minutes: ";
    cin >> Start_Time.hour;
    cin >> Start_Time.minute;

    while (Start_Time.hour < 00 || Start_Time.hour > 23 || Start_Time.minute < 00 || Start_Time.minute > 59 ) {

        cout << "Hours can only be 00 - 23. Minutes can only be 00 - 59. Please try again: ";
        cin >> Start_Time.hour;
        cin >> Start_Time.minute;

    }

    cout << "Please enter the duration of the event in minutes: ";
    cin >> Event_Time;


    j = (Start_Time.hour * 60) + Start_Time.minute + Event_Time;

    Start_Time.hour = j / 60;
    Start_Time.minute = j % 60;

    cout << "The time after the event has ended is: " << Start_Time.hour << ':' << Start_Time.minute;



    return 0;
}