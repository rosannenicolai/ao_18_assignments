cmake_minimum_required(VERSION 3.12)
project(2_11_1)

set(CMAKE_CXX_STANDARD 17)

add_executable(2_11_1 main.cpp)