#include <iostream>
#include <cmath>

using namespace std;
// Insert your functions here

void increment(int &intvar) {

    intvar += 1;

}



void increment(float &floatvar, int intvar) {

    floatvar += intvar;

}

void increment(int &intvar, int i) {

    intvar += i;
}

void increment(float &floatvar) {

    floatvar += 1;

}

int main(void) {
    int intvar = 0;
    float floatvar = 1.5;

    for(int i = 0; i < 10; i++) if(i % 2) {

        increment(intvar);
        increment(floatvar, sqrt(intvar));

    } else {

        increment(intvar,i); increment(floatvar);
}

    cout << intvar * floatvar << endl;

    return 0;
}