#include <iostream>

using namespace std;



int main(void) {

    int course, numberOfGrades;
    int **ptrarr;

    cout << "How many courses does the student follow?: ";
    cin >> course;

    float arithmeticMean = 0, final;
    ptrarr = new int *[course];


    for (int i = 0; i < course; i++) {

        cout << "Number of grades received?";
        cin >> numberOfGrades;

        ptrarr[i] = new int[numberOfGrades];

        cout << "Enter the grades";

        for (int j = 0; j < numberOfGrades; j++) {

            cin >> ptrarr[i][j];
            arithmeticMean += ptrarr[i][j];
        }

        arithmeticMean = arithmeticMean / numberOfGrades;
        cout.precision(3);
        cout << "Course "<< + i+1 << " final: " << arithmeticMean << ", grades: " << endl;
        final =+ arithmeticMean;
        arithmeticMean = 0;

    }

    cout.precision(3);
    final = final / course;
    cout << "Overal final " << final;
}