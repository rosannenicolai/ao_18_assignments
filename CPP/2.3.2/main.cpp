#include <iostream>

using namespace std;

int main(void) {

    double pi4 = 0.; long n; double addTotalValue = 0;

    cout << "Number of iterations? ";
    cin >> n;


    //ncalculator is the value for where each number (n) is
    //negativeChanger is to change the number into a negative
    //addTotalValue is to add up into the total value

    for (long ncalculator = 0; ncalculator < n; ncalculator++) {

        double negativeChanger = 1 / (2.0 * ncalculator + 1);

        if ((ncalculator % 2) == 1) {
            negativeChanger = negativeChanger * -1;
        }

        addTotalValue =(addTotalValue + negativeChanger);
        pi4 = addTotalValue;

    }


    cout.precision(20);
    cout << "Pi = " << (pi4 * 4.) << endl;

    return 0;
}
