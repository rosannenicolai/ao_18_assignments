#include <iostream>

using namespace std;

int main() {

    int num1, num2;
    int choice = 1;



    while (choice != 0) {

        //Displaying different options for calculator.
        cout << "MENU:" << endl << "0 - exit" << endl << "1 - addition" << endl << "2 - substraction" << endl
             << "3 - multiplication" << endl << "4 - division" << endl << "Your choice?" << endl;
        cin >> choice;



        //While-loop to check wether input is in the given range.
        while (choice < 0 || choice > 4) {
            cout << "Please choose an option mentioned above." << "Please enter again: ";
            cin >> choice;
        }


        if (choice == 0) {
            cout << "Bye!";
            return 0;
        }

        cout << "Enter two numbers: ";
        cin >> num1 >> num2;


        switch (choice) {


            //Addition
            case 1:
                cout << num1 + num2 << endl << endl;
                break;

                //Substraction
            case 2:
                cout << num1 - num2 << endl << endl;
                break;

                //Multiplication
            case 3:
                cout << num1 * num2 << endl << endl;
                break;

                //Division
            case 4:

                while (num2 == 0) {
                    cout << "Second number cannot be zero. Please try again";
                    cin >> num2;
                }

                cout << double(num1) / double(num2) << endl << endl;
                break;


        }

    }

    return 0;
}