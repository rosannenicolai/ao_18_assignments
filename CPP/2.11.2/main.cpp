#include <iostream>
#include <limits>

using namespace std;


struct TIME {

    int hour;
    int minute;
};

void calculateTimeDifference(struct TIME, struct TIME, struct TIME);


int i, j, k, l;

int main() {

    struct TIME t1, t2, difference;

    cout << "Using the 24h military format, please enter the start time in hours and minutes: ";
    cin >> t1.hour;
    cin >> t1.minute;

    while (t1.hour < 00 || t1.hour > 23 || t1.minute < 00 || t1.minute > 59 ) {

        cout << "Hours can only be 00 - 23. Minutes can only be 00 - 59. Please try again: ";
        cin >> t1.hour;
        cin >> t1.minute;

    }

    cout << "Using the 24h military format, please enter the end time in hours and minutes: ";
    cin >> t2.hour;
    cin >> t2.minute;

    while (t2.hour < 00 || t2.hour > 23 || t2.minute < 00 || t2.minute > 59 || t2.hour < t2.hour ) {

        cout << "Hours can only be 00 - 23. Minutes can only be 00 - 59. Please try again: ";
        cin >> t2.hour;
        cin >> t2.minute;

    }

    j = t1.hour * 60 + t1.minute;
    k = t2.hour * 60 + t2.minute;

    difference.minute = (k - j) % 60;
    difference.hour = (k - j) / 60;






    cout << "The time between the start and end is: " << difference.hour << ':' << difference.minute;



    return 0;
}